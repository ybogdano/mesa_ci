from repo_set.repo_set import (external_to_repo, checkout_externals, Mirrors,
                               RevisionSpecification, RepoSet, RepoNotCloned,
                               apply_patches)
