import argparse
import ast
import json
import multiprocessing
import os
import pwd
import shutil
import subprocess
import sys
import tarfile
import tempfile
import traceback
import datetime
import time

import queue
from threading import Thread

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))
from export import abspath2rsync_path
from utils.utils import reliable_url_open

HARDWARE_WHITELIST = [
    'bdw', 'bsw', 'builder', 'bxt', 'byt', 'cfl', 'g33',
    'g45', 'g965', 'glk', 'hsw', 'ilk', 'ivb', 'kbl', 'skl',
    'snb', 'gen9', 'gen9_zink', 'gen9atom', 'icl', 'icl_zink',
    'tgl', 'tgl_zink', 'dg2', 'dg2_zink', "xe_gen125", "xe_gen12",
    "mtl", "mtl_zink", "lnl", "lnl_zink"
]

DEFAULT_MAIN_BUILD_INFO = {
    "job": "n/a",
    "build_num": 0,
    "name": "n/a",
    "url": "n/a",
    "result_path": "n/a",
    "status": "n/a",
    "revisions": {},
    "components": [],
    "imported": False,
    "start_time": 0.0,
    "end_time": 0.0
}

DEFAULT_COMPONENT_INFO = {
    "name": "n/a",
    "status": "n/a",
    "url": "n/a",
    "trigger_time": 0.0,
    "start_time": 0.0,
    "machine": "n/a",
    "arch": "n/a",
    "shard": "n/a",
    "hardware": "n/a",
    "build": 0,
    "build_id": 0,
    "artifacts": [],
    "end_time": 0.0,
    "collect_time": 0.0
}

class BuildUploadQueue:
    """Manages asynchronous upload of compressed build metadata and
    component data to the results server

    """

    def __init__(self):
        self._queue = queue.Queue()
        self._getter_thread = Thread(target=BuildUploadQueue._getter, args=(self._queue,))

    def __enter__(self):
        self._getter_thread.start()

        return self

    def __exit__(self, type, value, tb):
        if tb:
            traceback.print_tb(tb)
        print("exiting builduploadqueue...")
        self._queue.put(None)
        self._getter_thread.join()
        print("joined getter thread")
        self._queue.join()
        print("joined queue")

    def upload_build_status(self, main_build_info, public_result_hosts, private_result_hosts, internal=False):
        self._queue.put({
            "func": "upload_build_status",
            "args": [dict(main_build_info), public_result_hosts, private_result_hosts],
            "kwargs": {"internal": internal}
        })

    def upload_component(self, main_build_info, invoke, public_result_hosts, private_result_hosts, internal=False):
        self._queue.put({
            "func": "upload_component",
            "args": [dict(main_build_info), invoke, public_result_hosts, private_result_hosts],
            "kwargs": {"internal": internal}
        })

    @staticmethod
    def _getter(build_queue):
        while True:
            try:
                queued_job = build_queue.get_nowait()
                if queued_job is None:
                    print("exiting getter")
                    build_queue.task_done()
                    return
                elif queued_job["func"] == "upload_build_status":
                    upload_build_status(*queued_job["args"], **queued_job["kwargs"])
                elif queued_job["func"] == "upload_component":
                    upload_component(*queued_job["args"], **queued_job["kwargs"])
                build_queue.task_done()
            except queue.Empty as err:
                time.sleep(60)
                continue

def upload_build_status(main_build_info, public_result_hosts, private_result_hosts, internal=False):
    """Update the status of a build in progress,
    or a build that has completed
    """
    build_info = dict(DEFAULT_MAIN_BUILD_INFO)
    build_info.update(main_build_info)
    if not internal and "revisions" in build_info:
        if "mesa_ci_internal" in build_info["revisions"]:
            build_info["revisions"].pop("mesa_ci_internal")

    _upload_build_tar(build_info, public_result_hosts + private_result_hosts)

def upload_component(main_build_info, invoke, public_result_hosts, private_result_hosts, internal=False):
    """Upload the contents of a
    component's full build_info file
    """

    if not internal and (invoke.options.hardware not in HARDWARE_WHITELIST):
        public_result_hosts = []

    build_info = dict(DEFAULT_MAIN_BUILD_INFO)
    build_info.update(main_build_info)
    if not internal and "revisions" in build_info:
        if "mesa_ci_internal" in build_info["revisions"]:
            build_info["revisions"].pop("mesa_ci_internal")
    comp_info = dict(DEFAULT_COMPONENT_INFO)
    comp_info_fname = os.path.basename(invoke.info_file())
    with tempfile.TemporaryDirectory() as tempd:
        test_files_dir = os.path.join(tempd, "test")

        subprocess.call([
            "rsync",
            "-r",
            "--mkpath",
            "--exclude=build_root",
            # this directory may not exist, this means that
            # the component never generated any results files
            # and can be safely ignored
            abspath2rsync_path(
                invoke.component_result_dir()
            ),
            abspath2rsync_path(
                invoke.info_file()
            ),
            tempd
        ], stderr=subprocess.DEVNULL)

        local_comp_info_path = os.path.join(tempd, comp_info_fname)
        with open(local_comp_info_path, "r") as fh:
            comp_info.update(json.load(fh))
        comp_info.update(filter(lambda i: i[0] in comp_info,  # i[0] is key
                                invoke.options.__dict__.items()
                                )
                         )
        if (
            comp_info["machine"] != DEFAULT_COMPONENT_INFO["machine"]
            and comp_info["status"] == "queued"
        ):
            comp_info["status"] = "building"
        build_info["components"] = [comp_info]

        if not os.path.isdir(test_files_dir):
            test_files_dir = ""

        _upload_build_tar(
            build_info,
            public_result_hosts + private_result_hosts,
            result_dir=test_files_dir,
            archive_hash=invoke.hash(str(time.time()))
        )

def _upload_build_tar(
    build_info,
    result_hosts,
    result_dir="",
    archive_hash=""
):
    archive_fname = (
        f"results_{build_info['name']}_{archive_hash}."
        f"{build_info['job']}."
        f"{build_info['build']}.tar"
    ).replace("/", "_")
    # limit threads for compression so master doesn't get overwhelmed
    threads = min(16, multiprocessing.cpu_count())

    with tempfile.TemporaryDirectory() as tempd:
        archive_fpath = os.path.join(tempd, archive_fname)
        xz_fpath = archive_fpath + ".xz"

        with open("build_info.json", "w") as fh:
            json.dump(build_info, fh)
        with tarfile.open(archive_fpath, "w") as tf:
            tf.add("build_info.json")

            if result_dir:
                for root, _, files in os.walk(result_dir):
                    for afile in files:
                        tf.add(os.path.join(root,afile), arcname=afile)

        with open(xz_fpath, "wb") as xz_file:
            subprocess.run(
                [
                    "xz",
                    "-T" + str(threads),
                    "-c",
                    archive_fpath
                ],
                stdout=xz_file,
                check=True
            )

        print(f"Results successfully compressed to: {xz_fpath}.")
        print(f"Uploading {xz_fpath} to results server...")

        _upload_tar(xz_fpath, result_hosts)


def _upload_tar(
    tar_file,
    result_hosts,
    user="jenkins",
    result_host_path="/tmp/mesa_ci_results",
    result_host_port=2222,
    retries=10,
    retry_freq=10, # seconds
    timeout=30 # seconds
):
    for host in result_hosts:
        for i in range(retries):
            try:
                cmd = [
                    'scp',
                    # Disable host key verification
                    '-o', 'StrictHostKeyChecking=no',
                    '-o', 'UserKnownHostsFile=/dev/null',
                    '-P', str(result_host_port),
                    tar_file,
                    f"{user}@{host}:{result_host_path}"
                ]
                subprocess.check_call(
                    cmd, timeout=timeout, stderr=subprocess.DEVNULL
                )
                break
            except (
                subprocess.CalledProcessError,
                subprocess.TimeoutExpired
            ):
                print(f"Tried this command and failed: {cmd}")
                time.sleep(retry_freq)
                if i < (retries - 1):
                    print("Retrying...")
                else:
                    print(f"Failed to export {tar_file}. Skipping...")
                continue
