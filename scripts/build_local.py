#!/usr/bin/env python3

import argparse
import sys
import os

parent_dir = os.path.split(os.path.dirname(os.path.abspath(sys.argv[0])))[1]
if parent_dir == "mesa_ci":
    sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                                 "../build_support"))
else:
    sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                                 "../repos/mesa_ci/build_support"))

from dependency_graph import DependencyGraph
from options import Options
from project_map import ProjectMap
from repo_set import RepoSet
from utils.command import run_batch_command, rmtree

class CsvChoice:
    """override to limit option to specific values"""
    def __init__(self, *args):
        self.values = args
    def __len__(self):
        return self.values.__len__()
    def __iter__(self):
        return self.values.__iter__()

    def __contains__(self, choice):
        # If we were not passed a string, it isn't contained here
        if type(choice) != str:
            return False
        result = True
        for i in choice.split(','):
            result &= i in self.values
        return result

class CsvAction(argparse.Action):
    """override to allow comma separated options"""
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, values.split(','))

def main():
    """build the project trees locally"""
    parser = argparse.ArgumentParser(description='Build projects locally.')

    parser.add_argument('--action', type=str, default=["build"],
                        choices=CsvChoice('build', 'clean', 'test'),
                        action=CsvAction,
                        help="Action to recurse with. 'build', 'clean' "\
                        "or 'test'. (default: %(default)s)")

    parser.add_argument('--project', dest='project', type=str, default="mesa",
                        help='project to build. (default: %(default)s)')
    parser.add_argument('--arch', dest='arch', type=str,
                        default='m64', choices=['m64', 'm32'],
                        help='arch to build. (default: %(default)s)')
    parser.add_argument('--config', type=str, default="debug",
                        choices=['release', 'debug'],
                        help="Release or Debug build. (default: %(default)s)")

    parser.add_argument('--type', type=str, default="developer",
                        choices=['developer', 'percheckin',
                                 'daily', 'release'],
                        help="category of tests to run. "\
                        "(default: %(default)s)")

    parser.add_argument('--env', type=str, default="",
                        help="If specified, overrides environment variable settings"
                        "EG: 'LIBGL_DEBUG=1 INTEL_DEBUG=perf'")
    parser.add_argument('--hardware', type=str, default='builder',
                        help="The hardware to be targeted for test "
                        "('builder', 'snbgt1', 'ivb', 'hsw', 'bdw'). "
                        "(default: %(default)s)")

    args = parser.parse_args()
    project = args.project

    # some build_local params are not handled by the Options, which is
    # used by other modules
    options = Options([sys.argv[0]])
    vdict = vars(args)
    del vdict["project"]

    options.__dict__.update(vdict)
    sys.argv = [sys.argv[0]] + options.to_list()

    if "clean" in args.action:
        rmtree(ProjectMap().build_root())

    graph = DependencyGraph(project, options)
    ready = graph.ready_builds()
    project_map = ProjectMap()
    while ready:
        for build_item in ready:
            graph.build_complete(build_item)
            proj_build_dir = project_map.project_build_dir(build_item.project)
            script = proj_build_dir + "/build.py"
            if os.path.exists(script):
                run_batch_command([sys.executable,
                                   script] +
                                  options.to_list())
        ready = graph.ready_builds()

main()
