import os
import sys

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                             "..", "build_support"))

from utils import check_gpu_hang

check_gpu_hang.check_gpu_hang(identify_test=False, create_failing_test=False)
